package com.project.whj.shortvideo.network.api;

import com.project.whj.shortvideo.mvp.model.bean.HomeBean;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

public interface ApiManger {

    /**
     * 首页精选
     * @return
     */
    @GET("v2/feed?")
    Observable<HomeBean> getFirstHomeData(@Query("num") int num);

    /**
     * 根据nextPageUrl 请求数据下一页
     * @param url
     * @return
     */
    @GET
    Observable<HomeBean> getMoreHomeData(@Url String url);
}
