package com.project.whj.shortvideo.mvp.model.bean;

import android.support.annotation.DrawableRes;

import com.flyco.tablayout.listener.CustomTabEntity;

public class TabEntity implements CustomTabEntity {

    private final String mTitle;
    private final int mSelectIcon;
    private final int mUnSelectIcon;

    public TabEntity(String title, @DrawableRes int selectIcon,
                     @DrawableRes int unSelectIcon){
        mTitle = title;
        mSelectIcon = selectIcon;
        mUnSelectIcon = unSelectIcon;
    }

    @Override
    public String getTabTitle() {
        return mTitle;
    }

    @Override
    public int getTabSelectedIcon() {
        return mSelectIcon;
    }

    @Override
    public int getTabUnselectedIcon() {
        return mUnSelectIcon;
    }

}
