package com.project.whj.shortvideo.view.recyclerview.adapter;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.project.whj.shortvideo.view.recyclerview.MultipleType;
import com.project.whj.shortvideo.view.recyclerview.ViewHolder;

import java.util.ArrayList;

/**
 * Created by wanghongjun on 2018/8/21.
 */

public abstract class CommonAdapter<T> extends RecyclerView.Adapter<ViewHolder> {
    private Context mContext;
    private ArrayList<T> mData;
    private MultipleType<T> mTypeSupport;
    private int mLayoutId=-1;
    private LayoutInflater mInflater;
    private OnItemClickListener mIntemClickListener;
    private OnItemLongClickListener mItemLongClickListener;


    public CommonAdapter (@NonNull Context context, @NonNull ArrayList<T> data,
                          @NonNull MultipleType<T> typeSupport ){
        this.mContext=context;
        this.mData=data;
        this.mTypeSupport=typeSupport;
        mInflater=LayoutInflater.from(mContext);
    }

    /**
     * 多布局问题
     * @param position
     * @return
     */
    @Override
    public int getItemViewType(int position) {
        if (mTypeSupport!=null){
            return mTypeSupport.getLayoutId(mData.get(position),position);
        }
        return super.getItemViewType(position);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (mTypeSupport!=null){
            mLayoutId=viewType;
        }
        //创建view
        View view=mInflater.inflate(mLayoutId,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        //绑定数据
        bindData(holder,mData.get(position),position);
        //条目点击事件
        if (mIntemClickListener!=null){
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mIntemClickListener.onItemClick(mData.get(position),position);
                }
            });
        }

        //长按点击事件
        if (mItemLongClickListener!=null){
            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    mItemLongClickListener.onIntemLongClick(mData.get(position),position);
                    return true;
                }
            });
        }


    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    /**
     * 将必要参数传递出去
     *
     * @param holder
     * @param data
     * @param positioin
     */
     protected abstract void bindData(ViewHolder holder, T data, int positioin);

    void setOnItemClickListener(OnItemClickListener onItemClickListener){
        mIntemClickListener = onItemClickListener;
    }

    void setOnItemLongClickListener(OnItemLongClickListener onItemLongClickListener){
        mItemLongClickListener = onItemLongClickListener;
    }



}
