package com.project.whj.shortvideo.network.client;

import com.project.whj.shortvideo.MyApplication;
import com.project.whj.shortvideo.network.api.ApiManger;
import com.project.whj.shortvideo.network.ip.IPConstant;
import com.project.whj.shortvideo.util.NetWorkUtil;
import com.project.whj.shortvideo.util.SharedPreferencesHelper;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.internal.Internal;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Url;

/**
 * retrofit 管理
 */
public class RetrofitManger {

    private static volatile RetrofitManger instance = null;

    private OkHttpClient client = null;
    private Retrofit retrofit = null;

    private RetrofitManger() {
    }

    public static RetrofitManger getInstance() {
        if (instance == null) {
            synchronized (RetrofitManger.class) {
                if (instance == null) {
                    instance = new RetrofitManger();
                }
            }
        }
        return instance;
    }

    public ApiManger getApiManger() {
        return getRetrofit().create(ApiManger.class);
    }

    private String getToken() {
        return (String) SharedPreferencesHelper.getInstance().
                getSharedPreference("token", "");
    }

    /**
     * 设置公共参数
     *
     * @return
     */
    private Interceptor addQuerParameterInterceptor() {
        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request originalRequest = chain.request();
                Request request;
                HttpUrl modifedUrl=originalRequest.url().newBuilder()
                        //提供自定的参数
                        .addQueryParameter("phoneSystem", "")
                        .addQueryParameter("phoneModel", "")
                        .build();
                request =originalRequest.newBuilder().url(modifedUrl).build();
                return chain.proceed(request);
            }
        };
    }

    /**
     * 设置头
     * @return
     */
    private Interceptor addHeaderIntercepter(){
        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request originRequest=chain.request();
                Request.Builder builder =originRequest.newBuilder()
                        //提供自定义的header
                        .header("token",getToken())
                        .method(originRequest.method(),originRequest.body());
                Request request=builder.build();
                return chain.proceed(request);
            }
        };
    }

    /**
     * 设置缓存
     * @return
     */
    private Interceptor addCacheInterceptor(){
        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request=chain.request();
                if (!NetWorkUtil.getInstance().isNetWorkAvailable(MyApplication.context)){
                    request=request.newBuilder()
                            .cacheControl(CacheControl.FORCE_CACHE)
                            .build();
                }
                Response response=chain.proceed(request);
                if (NetWorkUtil.getInstance().isNetWorkAvailable(MyApplication.context)){
                    int maxAge=0;
                    //有网络时，设置缓存超时时间0个小时，意思就是
                    //不读取缓存数据，只对get有用，post没有缓存
                    response.newBuilder()
                            .header("Cache-Control", "public, max-age=" + maxAge)
                            .removeHeader("Retrofit")// 清除头信息，因为服务器如果不支持，会返回一些干扰信息，不清除下面无法生效
                            .build();
                }else {
                    // 无网络时，设置超时为4周  只对get有用,post没有缓冲
                    int maxStale = 60 * 60 * 24 * 28;
                    response.newBuilder()
                            .header("Cache-Control", "public, only-if-cached, max-stale=" + maxStale)
                            .removeHeader("nyn")
                            .build();
                }
                return response;
            }
        };
    }


    private Retrofit getRetrofit() {
        if (retrofit == null) {
            synchronized (RetrofitManger.class) {
                if (retrofit == null) {
                    //添加一个log拦截器，打印所有的log
                    HttpLoggingInterceptor httpLoggingInterceptor=new HttpLoggingInterceptor();
                    //设置请求过滤的水平：body,basic,headers
                    httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

                    //设置 请求的缓存的大小跟位置
                    File cacheFile=new File(MyApplication.context.getCacheDir(),"cache");
                    Cache cache=new Cache(cacheFile,1024*1024*50);  //50Mb

                    client=new OkHttpClient.Builder()
                            .addInterceptor(addQuerParameterInterceptor())  //参数添加
                            .addInterceptor(addHeaderIntercepter()) //token过滤
                            .cache(cache)   //添加缓存
                            .connectTimeout(60L, TimeUnit.SECONDS)
                            .readTimeout(60L,TimeUnit.SECONDS)
                            .writeTimeout(60L,TimeUnit.SECONDS)
                            .build();

                    //获取Retrofit的实例
                    retrofit=new Retrofit.Builder()
                            .baseUrl(IPConstant.BASE_URL)
                            .client(client)
                            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                }
            }
        }

        return retrofit;
    }

}
