package com.project.whj.shortvideo.base;

import com.project.whj.shortvideo.base.IBaseView;

/**
 * Presenter 基类
 */
public interface IPresenter {


    void attachView(IBaseView mRootView);

    void detachView();
}
