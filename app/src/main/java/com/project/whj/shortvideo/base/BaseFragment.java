package com.project.whj.shortvideo.base;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.classic.common.MultipleStatusView;

/**
 * Fragment 基类
 */
public abstract class BaseFragment extends Fragment {
    /**
     * 视图是否加载完毕
     */
    private boolean isViewPrepare=false;
    /**
     * 数据是否加载过了
     */
    private boolean hasLoadData=false;
    /**
     * 多种状态的view的切换
     */
    public MultipleStatusView mLayoutStatusView=null;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (inflater==null){
            return null;
        }
        return inflater.inflate(getLayoutId(),null);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        isViewPrepare=true;
        initView(view);
        lazyLoadDataIfPrepared();
        //多种状态切换的view,重试点击事件
        if (mLayoutStatusView!=null){
            mLayoutStatusView.setOnClickListener(mRetryClickListener);
        }
        initEvent();//事件
    }

    private void lazyLoadDataIfPrepared(){
        if (getUserVisibleHint() && isViewPrepare && !hasLoadData){
            lazyLoad();
            hasLoadData=true;
        }
    }

    /**
     * Fragment可见的时候，调用
     * @param isVisibleToUser
     */
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser){
            lazyLoadDataIfPrepared();
        }
    }

    public View.OnClickListener mRetryClickListener=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            lazyLoad(); //懒加载
        }
    };

    /**
     * 加载布局
     * @return
     */
    @LayoutRes
    public abstract int getLayoutId();

    /**
     * 是初始化View
     */
   public abstract void initView(View view);

    /**
     * 事件处理
     */
   public abstract void initEvent();

    /**
     * 懒加载
     */
   public abstract void lazyLoad();

}
