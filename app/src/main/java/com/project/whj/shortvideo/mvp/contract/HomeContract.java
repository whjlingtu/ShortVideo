package com.project.whj.shortvideo.mvp.contract;

import com.project.whj.shortvideo.base.IBaseView;
import com.project.whj.shortvideo.base.IPresenter;
import com.project.whj.shortvideo.mvp.model.bean.HomeBean;

import java.util.ArrayList;

public interface HomeContract {

    interface View extends IBaseView{
        /**
         * 设置第一次请求的数据
         * @param bean
         */
        void setHomeData(HomeBean bean);

        /**
         * 设置更多加载数据
         * @param itemList
         */
        void setMoreData(ArrayList<HomeBean.IssueListBean.ItemListBean> itemList);

        /**
         * 设置显示错误信息
         */
        void showError(String msg,int errorCode);
    }

    interface Presenter extends IPresenter{
        /**
         * 获取首页精选数据
         * @param num
         */
        void requestHomeData(int num);

        /**
         * 加载更多数据
         */
        void loadMoreData();
    }
}
