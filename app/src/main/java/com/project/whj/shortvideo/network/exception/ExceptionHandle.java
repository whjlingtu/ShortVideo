package com.project.whj.shortvideo.network.exception;

import android.net.ParseException;

import com.google.gson.JsonParseException;
import com.orhanobut.logger.Logger;

import org.json.JSONException;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

/**
 * Created by wanghongjun on 2018/8/21.
 * desc:异常处理类
 */

public class ExceptionHandle {
    public int errorCode=ErrorStatus.UNKNOWN_ERROR;
    String errorMsg="请求失败，请稍后重试";

    private static volatile ExceptionHandle instance=null;

    public static ExceptionHandle getInstance(){
        if (instance==null){
            synchronized (ExceptionHandle.class){
                if (instance==null){
                    instance=new ExceptionHandle();
                }
            }
        }

        return instance;
    }

    public  String handleException(Throwable e){
        e.printStackTrace();
        if (e instanceof SocketTimeoutException) {//网络超时
            Logger.e("TAG", "网络连接异常: " + e.getMessage());
            errorMsg = "网络连接异常";
            errorCode = ErrorStatus.NETWORK_ERROR;
        } else if (e instanceof ConnectException) { //均视为网络错误
            Logger.e("TAG", "网络连接异常: " + e.getMessage());
            errorMsg = "网络连接异常";
            errorCode = ErrorStatus.NETWORK_ERROR;
        } else if (e instanceof JsonParseException
                    || e instanceof JSONException
                || e instanceof ParseException) {   //均视为解析错误
            Logger.e("TAG", "数据解析异常: " + e.getMessage());
            errorMsg = "数据解析异常";
            errorCode = ErrorStatus.SERVER_ERROR;
        } else if (e instanceof ApiException) {//服务器返回的错误信息
            errorMsg = e.getMessage();
            errorCode = ErrorStatus.SERVER_ERROR;
        } else if (e instanceof UnknownHostException) {
            Logger.e("TAG", "网络连接异常: " + e.getMessage());
            errorMsg = "网络连接异常";
            errorCode = ErrorStatus.NETWORK_ERROR;
        } else if (e instanceof IllegalArgumentException) {
            errorMsg = "参数错误";
            errorCode = ErrorStatus.SERVER_ERROR;
        } else {//未知错误
            try {
                Logger.e("TAG", "错误: " + e.getMessage());
            } catch ( Exception e1) {
                Logger.e("TAG", "未知错误Debug调试 ");
            }

            errorMsg = "未知错误，可能抛锚了吧~";
            errorCode = ErrorStatus.UNKNOWN_ERROR;
        }
        return errorMsg;
    }

}
