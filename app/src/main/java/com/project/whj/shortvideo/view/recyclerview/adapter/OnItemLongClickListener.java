package com.project.whj.shortvideo.view.recyclerview.adapter;

/**
 * Created by wanghongjun on 2018/8/21.
 */

public interface OnItemLongClickListener {
    boolean onIntemLongClick(Object obj,int position);
}
