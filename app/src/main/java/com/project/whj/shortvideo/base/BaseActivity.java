package com.project.whj.shortvideo.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.classic.common.MultipleStatusView;

/**
 * Activity 基类
 */
public abstract class BaseActivity extends AppCompatActivity {
    /**
     * 多种状态的View的切换
     */
    public MultipleStatusView mLayoutStatusView=null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layoutId());
        initData();
        initView();
        start();
        initListener();
    }

    private void initListener(){
        if (mLayoutStatusView!=null){
            mLayoutStatusView.setOnClickListener(mRetryClickListener);
        }
    }

    public View.OnClickListener mRetryClickListener=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            start();
        }
    };

    /**
     * 初始化数据
     */
   public abstract void initData();

    /**
     * 初始化 View
     */
    public abstract void initView();

    /**
     * 开始请求
     */
    public abstract void start();


    /**
     * 加载布局
     */
    public abstract int layoutId();

    /**
     * 打开软键盘
     * @param mEditText
     * @param mContext
     */
    public void openKeyBord(EditText mEditText, Context mContext){
        InputMethodManager imm=(InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(mEditText,InputMethodManager.RESULT_SHOWN);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    public void closeKeyBord(EditText mEditext,Context mContext){
        InputMethodManager imm=(InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mEditext.getWindowToken(),0);
    }
}
