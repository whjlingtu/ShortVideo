package com.project.whj.shortvideo.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;

import com.project.whj.shortvideo.mvp.model.bean.HomeBean;
import com.project.whj.shortvideo.view.recyclerview.MultipleType;
import com.project.whj.shortvideo.view.recyclerview.ViewHolder;
import com.project.whj.shortvideo.view.recyclerview.adapter.CommonAdapter;

import java.util.ArrayList;

/**
 * Created by wanghongjun on 2018/8/22.
 * desc: 首页精选的Adapter
 */

public class HomeAdapter extends CommonAdapter<HomeBean.IssueListBean.ItemListBean> {

    private Context context;
    private ArrayList<HomeBean.IssueListBean.ItemListBean> data;

    int bannerItemSize=0;

    private int ITEM_TYPE_BANNER=1; //Banner 类型
    private int ITEM_TYPE_TEXT_HEADER=2;    //textHeader
    private int ITEM_TYPE_CONTENT=3;    //item


    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    public HomeAdapter(@NonNull Context context, @NonNull ArrayList<HomeBean.IssueListBean.ItemListBean> data, @NonNull MultipleType<HomeBean.IssueListBean.ItemListBean> typeSupport) {
        super(context, data, typeSupport);
    }

    @Override
    protected void bindData(ViewHolder holder, HomeBean.IssueListBean.ItemListBean data, int positioin) {

    }
}
