package com.project.whj.shortvideo.base;

import android.support.annotation.NonNull;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class BasePresenter implements IPresenter {

    public IBaseView mRootView=null;
    private CompositeDisposable compositeDisposable=new CompositeDisposable();


    @Override
    public void attachView(IBaseView mRootView) {
        this.mRootView=mRootView;
    }

    @Override
    public void detachView() {
        mRootView=null;

        //保证activity结束时取消正在执行的订阅
        if (!compositeDisposable.isDisposed()){
            compositeDisposable.clear();
        }
    }

    private boolean isViewAttached(){
        return mRootView!=null;
    }

    public void addSubscription(Disposable disposable){
        compositeDisposable.add(disposable);
    }

    public void checkViewAttached(){
        if (!isViewAttached()) throw new RuntimeException("Please call IPresenter.attachView(IBaseView) before"
                + " requesting data to the IPresenter");
    }


}
