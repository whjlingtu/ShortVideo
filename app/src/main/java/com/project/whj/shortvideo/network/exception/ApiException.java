package com.project.whj.shortvideo.network.exception;

/**
 * Created by wanghongjun on 2018/8/21.
 */

public class ApiException extends RuntimeException {

    private int code;

    public ApiException(Throwable throwable ,int code){
        super(throwable);
        this.code=code;
    }

    public ApiException(String message){
        super(new Throwable(message));
    }

}
