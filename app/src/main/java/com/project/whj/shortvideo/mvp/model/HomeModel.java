package com.project.whj.shortvideo.mvp.model;

import com.project.whj.shortvideo.mvp.model.bean.HomeBean;
import com.project.whj.shortvideo.network.client.RetrofitManger;
import com.project.whj.shortvideo.rx.scheduler.SchedulerUtils;

import io.reactivex.Observable;

/**首页精选model
 *
 */
public class HomeModel {

    private static volatile HomeModel instance=null;

    private HomeModel(){}

    public static HomeModel getInstance(){
        if (instance==null){
            synchronized (HomeModel.class){
                if (instance==null){
                    instance=new HomeModel();
                }
            }
        }
        return instance;
    }

    /**
     * 获取首页Banner数据
     * @param num
     * @return
     */
    public Observable<HomeBean> requestHomeData(int num){
        return RetrofitManger.getInstance().getApiManger().getFirstHomeData(num)
                .compose(SchedulerUtils.ioToMain());
    }

    public Observable<HomeBean> loadMoreData(String url){
        return RetrofitManger.getInstance().getApiManger().getMoreHomeData(url)
                .compose(SchedulerUtils.ioToMain());
    }


}
