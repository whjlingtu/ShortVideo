package com.project.whj.shortvideo.mvp.presenter;

import com.project.whj.shortvideo.base.BasePresenter;
import com.project.whj.shortvideo.mvp.model.HomeModel;
import com.project.whj.shortvideo.mvp.model.bean.HomeBean;
import com.project.whj.shortvideo.mvp.contract.HomeContract;
import com.project.whj.shortvideo.network.exception.ExceptionHandle;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;

public class HomePresenter extends BasePresenter implements HomeContract.Presenter {

    private HomeContract.View view;

    private HomeBean bannerHomeBean;

    private String nextPageUrl = null;

    public HomePresenter(HomeContract.View view) {
        this.view = view;
    }


    @Override
    public void requestHomeData(final int num) {
        //检测是否绑定View
        checkViewAttached();
        if (mRootView != null) {
            mRootView.showLoading();
        }
        final Disposable disposable = HomeModel.getInstance().requestHomeData(num)
                .flatMap(new Function<HomeBean, ObservableSource<?>>() {
                    @Override
                    public ObservableSource<?> apply(HomeBean homeBean) throws Exception {

                        //过滤掉 Banner2(包含广告，等不需要的Type)
                        List<HomeBean.IssueListBean.ItemListBean> bannerItemList = homeBean.getIssueList().get(0).getItemList();

                        for (int i = 0; i < bannerItemList.size(); i++) {
                            HomeBean.IssueListBean.ItemListBean item = bannerItemList.get(i);
                            if (item.getType().equals("banner2") || item.getType().equals("horizontalScrollCard")) {
                                bannerItemList.remove(item);
                            }
                        }

                        bannerHomeBean = homeBean;    //记录第一页是当做banner数据

                        //根据nextPageUrl 请求下一页数据
                        HomeModel.getInstance().loadMoreData(homeBean.getNextPageUrl());
                        return Observable.just(homeBean);
                    }
                })
                .subscribe(new Consumer<Object>() {
                    @Override
                    public void accept(Object o) throws Exception {
                        mRootView.dismissLoading();
                        HomeBean homeBean = (HomeBean) o;
                        nextPageUrl = homeBean.getNextPageUrl();

                        //过滤掉 Banner2(包含广告，等不需要的Type)
                        List<HomeBean.IssueListBean.ItemListBean> newBannerItemList = homeBean.getIssueList().get(0).getItemList();
                        for (int i = 0; i < newBannerItemList.size(); i++) {
                            HomeBean.IssueListBean.ItemListBean item = newBannerItemList.get(i);
                            if (item.getType().equals("banner2") || item.getType().equals("horizontalScrollCard")) {
                                newBannerItemList.remove(item);
                            }
                        }

                        //重新赋值 Banner
                        if (bannerHomeBean != null) {
                            bannerHomeBean.getIssueList().get(0).setCount(bannerHomeBean.getIssueList().get(0).getItemList().size());
                        }

                        //赋值过滤后的数据+banner数据
                        if (bannerHomeBean != null) {
                            bannerHomeBean.getIssueList().get(0).getItemList().addAll(newBannerItemList);
                        }

                        view.setHomeData(bannerHomeBean);

                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        if (mRootView != null) {
                            mRootView.dismissLoading();
                            view.showError(ExceptionHandle.getInstance().handleException(throwable),
                                    ExceptionHandle.getInstance().errorCode);
                        }
                    }
                });
        addSubscription(disposable);
    }

    @Override
    public void loadMoreData() {

    }


}
