package com.project.whj.shortvideo.ui.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.project.whj.shortvideo.R;
import com.project.whj.shortvideo.base.BaseActivity;
import com.project.whj.shortvideo.mvp.model.bean.TabEntity;
import com.project.whj.shortvideo.ui.fragment.DiscoveryFragment;
import com.project.whj.shortvideo.ui.fragment.HomeFragment;
import com.project.whj.shortvideo.ui.fragment.HotFragment;
import com.project.whj.shortvideo.ui.fragment.MineFragment;


import java.util.ArrayList;

public class MainActivity extends BaseActivity {

    private String[] bottomTitle={"每日精选","发现","热门","我的"};

    //未选中的图标
    private int[] mIconUnSelectIds=new int[]{
            R.mipmap.ic_home_normal,
            R.mipmap.ic_discovery_normal,
            R.mipmap.ic_hot_normal,
            R.mipmap.ic_mine_normal
    };

    //选中的图标
    private int[] mIconSelectIds=new int[]{
            R.mipmap.ic_home_selected,
            R.mipmap.ic_discovery_selected,
            R.mipmap.ic_hot_selected,
            R.mipmap.ic_mine_selected
    };

    private ArrayList<CustomTabEntity> mTabEnities=new ArrayList<CustomTabEntity>();
    private CommonTabLayout mTabLayout;
    private int mIndex=0;
    private FragmentManager fragmentManager;

    //底部Framgment
    private HomeFragment mHomeFragment; //首页
    private DiscoveryFragment mDiscoveryFragment;//发现
    private HotFragment mHotFragment;  //热点
    private MineFragment mMineFragment;  //我的


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (savedInstanceState!=null){
            mIndex=savedInstanceState.getInt("currTabIndex");
        }
        super.onCreate(savedInstanceState);
        initTab();
        mTabLayout.setCurrentTab(mIndex);   //设置当前Tab
        switchFragment(mIndex);
    }




    @Override
    public int layoutId() {
        return R.layout.activity_main;
    }

    @Override
    public void initData() {
        fragmentManager = getSupportFragmentManager();

    }

    @Override
    public void initView() {
        mTabLayout = (CommonTabLayout)findViewById(R.id.tab_layout);
    }


    @Override
    public void start() {

    }

    /**
     * 切换Fragment
     * @param index
     */
    private void switchFragment(int index){
        FragmentTransaction transaction=(FragmentTransaction)fragmentManager.beginTransaction();
        hideFragments(transaction);
        switch (index){
            case 0: //首页
                if (mHomeFragment!=null){
                    transaction.show(mHomeFragment);
                }else {
                    mHomeFragment=HomeFragment.getInStance(bottomTitle[index]);
                    transaction.add(R.id.fl_container,mHomeFragment,"home");
                    transaction.show(mHomeFragment);
                }
                break;
            case 1: //发现
                if (mDiscoveryFragment!=null){
                    transaction.show(mDiscoveryFragment);
                }else {
                    mDiscoveryFragment=DiscoveryFragment.getInstance(bottomTitle[index]);
                    transaction.add(R.id.fl_container,mDiscoveryFragment,"discovery");
                    transaction.show(mDiscoveryFragment);
                }
                break;
            case 2: //热门
                if (mHotFragment!=null){
                    transaction.show(mHotFragment);
                }else {
                    mHotFragment=HotFragment.getInstance(bottomTitle[index]);
                    transaction.add(R.id.fl_container,mHotFragment,"hot");
                    transaction.show(mHotFragment);
                }
                break;
            case 3://我的
                if (mMineFragment!=null){
                    transaction.show(mMineFragment);
                }else {
                    mMineFragment=MineFragment.getInstance(bottomTitle[index]);
                    transaction.add(R.id.fl_container,mMineFragment,"mine");
                    transaction.show(mMineFragment);
                }
                break;
        }
        mIndex=index;
        mTabLayout.setCurrentTab(mIndex);
        transaction.commitAllowingStateLoss();
    }

    /**
     * 隐藏所有的fragment
     * @param transaction
     */
    private void hideFragments(FragmentTransaction transaction){
        if (mHomeFragment!=null){
            transaction.hide(mHomeFragment);
        }
        if (mDiscoveryFragment!=null){
            transaction.hide(mDiscoveryFragment);
        }
        if (mHotFragment!=null){
            transaction.hide(mHotFragment);
        }
        if (mMineFragment!=null){
            transaction.hide(mMineFragment);
        }
    }



    /**
     * 初始化底部导航
     */
    private void initTab() {
        for (int i=0;i<bottomTitle.length;i++){
            TabEntity tabEnity=new TabEntity(bottomTitle[i],
                    mIconSelectIds[i],mIconUnSelectIds[i]);
            mTabEnities.add(tabEnity);
        }
        //为Tab赋值
        mTabLayout.setTabData(mTabEnities);
        mTabLayout.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                //选中的Tab,一般用于切换Fragment
                switchFragment(position);
            }

            @Override
            public void onTabReselect(int position) {

            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mTabLayout!=null){
            outState.putInt("currTabIndex",mIndex);
        }
    }
}
