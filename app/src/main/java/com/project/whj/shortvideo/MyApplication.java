package com.project.whj.shortvideo;

import android.app.Application;
import android.content.Context;

import com.orhanobut.logger.*;
import com.orhanobut.logger.BuildConfig;

public class MyApplication extends Application {

    public   static Context context;


    @Override
    public void onCreate() {
        super.onCreate();
        context=getApplicationContext();
        initConfig();
    }

    /**
     * 初始化配置
     */
    private void initConfig(){
        PrettyFormatStrategy formatStrategy=PrettyFormatStrategy.newBuilder()
                .showThreadInfo(false)  // 隐藏线程信息 默认：显示
                .methodCount(0)         // 决定打印多少行（每一行代表一个方法）默认：2
                .methodOffset(7)        // (Optional) Hides internal method calls up to offset. Default 5
                .tag("whj_tag")   // (Optional) Global tag for every log. Default PRETTY_LOGGER
                .build();
        Logger.addLogAdapter(new AndroidLogAdapter(formatStrategy) {
            @Override
            public boolean isLoggable(int priority, String tag) {
                return BuildConfig.DEBUG;
            }
        });
    }
}
