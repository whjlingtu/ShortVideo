package com.project.whj.shortvideo.ui.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;

import com.project.whj.shortvideo.R;
import com.project.whj.shortvideo.base.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class MineFragment extends BaseFragment {

    private static volatile MineFragment instance;
    private static String mTitle;

    public static MineFragment getInstance(String title){
        if (instance==null){
            synchronized (MineFragment.class){
                if (instance==null){
                    instance=new MineFragment();
                    Bundle bundle=new Bundle();
                    instance.setArguments(bundle);
                    instance.mTitle=title;
                }
            }
        }
        return instance;
    }

    @Override
   public int getLayoutId() {
        return R.layout.fragment_mine;
    }

    @Override
   public void initView(View view) {

    }

    @Override
   public void initEvent() {

    }

    @Override
   public void lazyLoad() {

    }
}
