package com.project.whj.shortvideo.view.recyclerview;

import android.annotation.SuppressLint;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


/**
 * Created by wanghongjun on 2018/8/21.
 */

public class ViewHolder extends RecyclerView.ViewHolder {

    //用于缓存已找的界面
    private SparseArray<View> mView = new SparseArray<>();
    public View itemView = null;

    public ViewHolder(View itemView) {
        super(itemView);
        this.itemView = itemView;
    }

    public View getView(int viewId) {
        //对已经有的view做缓存
        View view = mView.get(viewId);
        //使用的缓存的方式减少findViewById的次数
        if (view == null) {
            view = itemView.findViewById(viewId);
            if (view!=null){
                mView.put(viewId, view);
            }
        }
        return view;
    }

    public ViewGroup getViewGroup(int viewId) {
        //对已有的view做缓存
        View view = mView.get(viewId);
        //使用的缓存的方式减少findViewById的次数
        if (view == null) {
            view = itemView.findViewById(viewId);
            if (view!=null){
                mView.put(viewId,view);
            }
        }
        return (ViewGroup) view;
    }

    //通用的功能进行封装 设置文本 设置条目 点击事件 设置图片
    @SuppressLint("SetTextI18n")
    public ViewHolder setText(int viewId,CharSequence text){
        TextView view=(TextView)getView(viewId);
        view.setText(""+text);
        return this;
    }

    public ViewHolder setHintText(int viewId,CharSequence text){
        TextView view=(TextView)getView(viewId);
        view.setHint(""+text);
        return this;
    }

    /**
     * 加载本地图片
     * @param viewId
     * @param resId
     * @return
     */
    public ViewHolder setImageResource(int viewId,int resId){
        ImageView iv=(ImageView)getView(viewId);
        iv.setImageResource(resId);
        return this;
    }

    public ViewHolder setImagePath(int viewId,HolderImageLoader imageLoader){
        ImageView iv=(ImageView)getView(viewId);
        imageLoader.loadImage(iv,imageLoader.path);
        return this;
    }

    abstract class HolderImageLoader{

        String path="";

         HolderImageLoader(String path){
             this.path=path;
         }

        abstract void loadImage(ImageView iv,String path);
    }

    public ViewHolder setViewVisbility(int viewId,int visibility){
        View view = (View) getView(viewId);
        view.setVisibility(visibility);
        return this;
    }

    /**
     * 设置条目点击事假
     * @param listener
     */
    public void setOnItemClickListener(View.OnClickListener listener){
        itemView.setOnClickListener(listener);
    }

    /**
     * 设置条目长按事件
     * @param longClickListener
     */
    public void setOnItemLongClickListener(View.OnLongClickListener longClickListener){
        itemView.setOnLongClickListener(longClickListener);
    }


}
