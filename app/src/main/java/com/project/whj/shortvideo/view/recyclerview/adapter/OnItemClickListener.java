package com.project.whj.shortvideo.view.recyclerview.adapter;

/**
 * Created by wanghongjun on 2018/8/21.
 */

public interface OnItemClickListener {
    void onItemClick(Object obj,int position);
}
