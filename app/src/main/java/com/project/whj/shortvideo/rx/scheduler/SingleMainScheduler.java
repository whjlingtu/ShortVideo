package com.project.whj.shortvideo.rx.scheduler;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by wanghongjun on 2018/8/21.
 */

public class SingleMainScheduler extends BaseScheduler {
    private SingleMainScheduler() {
        super(Schedulers.computation(), AndroidSchedulers.mainThread());
    }
}
