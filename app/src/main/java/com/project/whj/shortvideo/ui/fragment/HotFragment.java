package com.project.whj.shortvideo.ui.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;

import com.project.whj.shortvideo.R;
import com.project.whj.shortvideo.base.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class HotFragment extends BaseFragment {

    private static volatile HotFragment instance;
    private String mTitle;

    public static HotFragment getInstance(String title){
        if (instance==null){
            synchronized (HotFragment.class){
                if (instance==null){
                    instance=new HotFragment();
                    Bundle bundle=new Bundle();
                    instance.setArguments(bundle);
                    instance.mTitle=title;
                }
            }
        }
        return instance;
    }

    @Override
   public int getLayoutId() {
        return R.layout.fragment_hot;
    }

    @Override
   public void initView(View view) {

    }

    @Override
   public void initEvent() {

    }

    @Override
    public void lazyLoad() {

    }
}
