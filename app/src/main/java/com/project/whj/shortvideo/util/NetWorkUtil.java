package com.project.whj.shortvideo.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URI;
import java.net.URL;
import java.util.Enumeration;
import java.util.List;

/**
 * 网络工具类
 */
public class NetWorkUtil {
    private int NET_CNNT_BAIDU_OK = 1; //网络有效
    private int NET_CNNT_TIMEOUT = 2; //网络没有效
    private int NET_NOT_PREPARE = 3; //网络不可读
    private int NET_ERROR = 4; //网络错误
    private int TIMOUT = 3000; //超时

    private static volatile NetWorkUtil instance = null;

    private NetWorkUtil() {
    }

    public static NetWorkUtil getInstance() {
        if (instance == null) {
            synchronized (NetWorkUtil.class) {
                if (instance == null) {
                    instance = new NetWorkUtil();
                }
            }
        }
        return instance;
    }


    /**
     * 检测网络是否有效
     *
     * @param context
     * @return
     */
    public boolean isNetWorkAvailable(Context context) {
        ConnectivityManager manager = (ConnectivityManager) context.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        return !(null == networkInfo || !networkInfo.isAvailable());
    }

    /**
     * 得到Ip地址
     *
     * @return
     */
    public String getLocalIpAddress() {
        String ret = "";
        try {
            Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
            while (en.hasMoreElements()) {
                Enumeration<InetAddress> enumIpAddress = en.nextElement().getInetAddresses();
                while (enumIpAddress.hasMoreElements()) {
                    InetAddress netAddress = enumIpAddress.nextElement();
                    if (!netAddress.isLoopbackAddress()) {
                        ret = netAddress.getHostAddress();
                    }
                }
            }
        } catch (SocketException ex) {
            ex.printStackTrace();
        }
        return ret;
    }

    /**
     * ping "http://www.baidu.com"
     *
     * @return
     */
    private boolean pingNetWork() {
        boolean result = false;
        HttpURLConnection httpUrl = null;
        try {
            httpUrl = (HttpURLConnection) new URL("http://www.baidu.com")
                    .openConnection();
            httpUrl.setConnectTimeout(TIMOUT);
            httpUrl.connect();
            result = true;
        } catch (IOException e) {

        } finally {
            if (null != httpUrl) {
                httpUrl.disconnect();
            }
        }
        return result;
    }

    /**
     * 检测是否是3G
     *
     * @param context
     * @return
     */
    public boolean is3G(Context context) {
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (manager == null) {
            return false;
        }
        NetworkInfo info = manager.getActiveNetworkInfo();
        return info != null && info.getType() == ConnectivityManager.TYPE_MOBILE;
    }

    /**
     * 检测是否是Wifi
     *
     * @param context
     * @return
     */
    public boolean isWifi(Context context) {
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (manager == null) {
            return false;
        }
        NetworkInfo info = manager.getActiveNetworkInfo();
        return info != null && info.getType() == ConnectivityManager.TYPE_WIFI;
    }

    /**
     * 检测是否是2G
     *
     * @param context
     * @return
     */
    public boolean is2G(Context context) {
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (manager == null) {
            return false;
        }
        NetworkInfo info = manager.getActiveNetworkInfo();
        return info != null && (info.getSubtype() == TelephonyManager.NETWORK_TYPE_EDGE
                || info.getSubtype() == TelephonyManager.NETWORK_TYPE_GPRS || info
                .getSubtype() == TelephonyManager.NETWORK_TYPE_CDMA);
    }

    /**
     * 启用wifi
     *
     * @param context
     * @return
     */
    public boolean isWifiEnabled(Context context) {
        ConnectivityManager mgrConn = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (mgrConn == null) {
            return false;
        }
        TelephonyManager mgrTel = (TelephonyManager)
                context.getSystemService(Context.TELEPHONY_SERVICE);
        if (mgrTel==null){
            return false;
        }
        return mgrConn.getActiveNetworkInfo() != null && mgrConn
                .getActiveNetworkInfo().getState() == NetworkInfo.State.CONNECTED || mgrTel
                .getNetworkType() == TelephonyManager.NETWORK_TYPE_UMTS;
    }

}
