package com.project.whj.shortvideo.base;

public interface IBaseView {
    void showLoading();
    void dismissLoading();
}
