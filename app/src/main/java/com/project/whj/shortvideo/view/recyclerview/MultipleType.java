package com.project.whj.shortvideo.view.recyclerview;

import android.widget.TextView;

/**
 * Created by wanghongjun on 2018/8/21.
 */

public interface MultipleType<T> {
    int getLayoutId(T item, int position);
}
