package com.project.whj.shortvideo.ui.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.classic.common.MultipleStatusView;
import com.orhanobut.logger.Logger;
import com.project.whj.shortvideo.R;
import com.project.whj.shortvideo.base.BaseFragment;
import com.project.whj.shortvideo.mvp.model.bean.HomeBean;
import com.project.whj.shortvideo.mvp.contract.HomeContract;
import com.project.whj.shortvideo.mvp.presenter.HomePresenter;
import com.project.whj.shortvideo.util.StatusBarUtil_V1;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends BaseFragment implements HomeContract.View {

    private static volatile HomeFragment instance;
    private static String mTitle = null;
    private SmartRefreshLayout mSmartRefreshLayout;
    private MaterialHeader mMaterialHeader;
    private Toolbar mToolbar;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager linearLayoutManager;
    private boolean isRefresh = false;
    private boolean loadingMore = false;
    private int num=1;
    private HomePresenter presenter1;


    public static HomeFragment getInStance(String title) {
        if (instance == null) {
            synchronized (HomeFragment.class) {
                if (instance == null) {
                    instance = new HomeFragment();
                    Bundle bundle = new Bundle();
                    instance.setArguments(bundle);
                    instance.mTitle = title;
                }
            }
        }
        return instance;
    }


    @Override
    public int getLayoutId() {
        return R.layout.fragment_home;
    }

    @Override
    public void initView(View view) {
        presenter1 = new HomePresenter(this);
        presenter1.attachView(this);

        mSmartRefreshLayout = (SmartRefreshLayout) view.findViewById(R.id.mRefreshLayout);
        mLayoutStatusView = (MultipleStatusView) view.findViewById(R.id.multipleStatusView);
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.mRecyclerView);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        //内容跟随偏移
        mSmartRefreshLayout.setEnableHeaderTranslationContent(true);
        //Heard
        mMaterialHeader = (MaterialHeader) mSmartRefreshLayout.getRefreshHeader();
        //打开下拉刷新区域看背景
        mMaterialHeader.setShowBezierWave(true);
        //设置下拉主题颜色
        mSmartRefreshLayout.setPrimaryColorsId(R.color.color_light_black, R.color.color_title_bg);

        //状态栏和间距处理
        StatusBarUtil_V1.darkMode(getActivity());
        StatusBarUtil_V1.setPaddingSmart(getContext(),mToolbar);

    }

    @Override
    public void initEvent() {
        mSmartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {

            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                isRefresh = true;
                //刷新数据
                presenter1.requestHomeData(num);
            }
        });
        //RecyclerView滚动的时候调用
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    int childCount = mRecyclerView.getChildCount();
                    int itemCount = mRecyclerView.getLayoutManager().getItemCount();
                    LinearLayoutManager manager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
                    int firstVisibleItem=manager.findFirstVisibleItemPosition();
                    if (firstVisibleItem+childCount==itemCount){
                        if (!loadingMore){
                            loadingMore=true;
                            //加载更多数据
                        }
                    }
                }
            }

            //recyclerview滚动时调用
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });
    }

    @Override
   public void lazyLoad() {
        presenter1.requestHomeData(num);
    }


    @Override
    public void setHomeData(HomeBean bean) {
        if (mLayoutStatusView!=null) mLayoutStatusView.showContent();
        Logger.d(bean);
    }

    @Override
    public void setMoreData(ArrayList<HomeBean.IssueListBean.ItemListBean> itemList) {

    }

    @Override
    public void showError(String msg, int errorCode) {
        Toast.makeText(getContext(),msg,Toast.LENGTH_SHORT).show();
    }


    /**
     * 显示Loading(下拉刷新的时候不现实Loading)
     */
    @Override
    public void showLoading() {
        if (!isRefresh){
            isRefresh=false;
            mLayoutStatusView.showLoading();
        }
    }

    /**
     * 隐藏Loading
     */
    @Override
    public void dismissLoading() {
        mSmartRefreshLayout.finishRefresh();
    }
}
